\begin{zed}
[MDATA]
\end{zed}


\begin{zed}
CTYPE ::= RM | SC | IDA | SDA | TD | RC | MD | ML | LP
\end{zed}


\begin{zed}
RTYPE ::= MR | CD | EDP | ND | MDD
\end{zed}


\begin{zed}
CMODE ::= COF | CON
\end{zed}


\begin{zed}
PARAM ::= 
  LLDHiOff 
  | LLDHiOn | TMOff | TMOn | HiV50 | HiV45 | HiV40 | HiVOff | RU | TS18 | TS13
\end{zed}


\begin{zed}
TIME == \nat %sin
\end{zed}

$BIN$ represents any binary type.

\begin{zed}
BIN ::= no | yes
\end{zed}

\begin{schema}{Memory}
memp, memd: \seq MDATA \\
mdp, mep, ped: \nat
\end{schema}

\begin{zed}
Time == [ctime:TIME]
\end{zed}

\begin{schema}{Status}
acquiring, waiting, sending, dumping, waitsignal: BIN \\
mode: CMODE \\
ccmd: CTYPE
\end{schema}

The state schema is the inclusion of the previous schema.

\begin{zed}
ExpState == [Memory; Time; Status]
\end{zed}

The initial state schema, which actually represents an infinite set of states, sets reasonable values for some of the state variables.

\begin{schema}{ExpInitState}
ExpState
\where
memp = memd = \langle \rangle \\
mdp = mep = ped = 0 \\
acquiring = waiting = sending = dumping = waitsignal = no
\end{schema}

The state invariants of the system are described in the following schema.

\begin{schema}{StateInvariants}
ExpState
\where
\# memp \leq 5*43 \\
\# memd \leq 43 \\
mep \in 0 \upto 43 \\
ped \in 0 \upto 43 \\
mdp \in 0 \upto 5*43 \\
acquiring = no \implies waitsignal = no
\end{schema}

\subsection{Operations}

For the sake of simplicity we assume that, seen from a rather abstract level, commands are sent by OBDH to EXP in four steps (regardless of how many low-level data packages have to be sent):

\begin{enumerate}
\item OBDH initiates a command; 

\item OBDH sends the command type;

\item OBDH sends the parameters (if needed) according to the type;

\item OBDH finishes the command.
\end{enumerate}

\noindent EXP will synchronize with OBDH as follows: 

\begin{enumerate}
\item EXP will wait for OBDH to initiate a command;

\item EXP will wait for a command type;

\item EXP will wait for the end of the command;

\item EXP will execute one of several internal operations depending on the command type received in the second step.

Note that we do not model the reception of parameters: we simply assume that that happens in some way.
\end{enumerate}

The operations corresponding to the first three steps are described in section \ref{stf} and the internal operations (cf. forth step) are described in sections \ref{soo} and \ref{coo}. However, the next section (\ref{tro}) shows some time-related operations since some of the other operations have time requisites.

EXP also has to synchronize with the satellite's sensors for data acquisition. This is shown in section \ref{dao}.

It must be remarked that all the operations, except when is explicitly mentioned, constitute the interface that EXP exposes to OBDH and other components such as sensors and the clock.


\subsubsection{\label{tro}Time-Related Operations}

$Tick$ is an external operation performed by EXP's hardware and represents time advancing one time unit. Somehow, either EXP receives that new value or the hardware modifies one of EXP's memory cells. This is irrelevant from this level of abstraction.

\begin{zed}
Tick == [\Delta ExpState; \Xi Memory; \Xi Status | ctime' = ctime + 1]
\end{zed}

When EXP receives the beginning of a new command from OBDH, it sets a timer to timeout after 500 ms. $Timeout500ms$ represents that timeout (in other words the (external) timer shall call $Timeout500ms$ when it finishes). After the execution of this operation EXP will wait anymore for the rest of the message. $Set500msTimer$ and $Stop500Timer$ constitute the interface EXP shall use to set and stop the timer, we left them sub-specified.

\begin{multicols}{2}
\begin{schema}{Timeout500ms}
\Delta ExpState; \Xi Memory; \Xi Time
\where
waiting' = no \\
acquiring' = acquiring \\
sending' = sending \\
dumping' = dumping \\
waitsignal' = waitsignal \\
mode' = mode \\
ccmd' = ccmd
\end{schema}

\begin{schema}{Set500msTimer}
x:\num
\end{schema}

\begin{schema}{Stop500msTimer}
x:\num
\end{schema}
\end{multicols}

The other time requisite for EXP regards collecting experiment's data. EXP shall issue an interruption to the telescope hardware every 700 ms. We proceed in a similar fashion.

\begin{multicols}{2}
\begin{schema}{Timeout700ms}
\Delta ExpState; \Xi Memory; \Xi Time
\where
waitsignal' = no \\
sending' = sending \\
waiting' = waiting \\
acquiring' = acquiring \\
dumping' = dumping \\
mode' = mode \\
ccmd' = ccmd
\end{schema}

\begin{schema}{Set700msTimer}
x:\num
\end{schema}

\begin{schema}{Stop700msTimer}
x:\num
\end{schema}
\end{multicols}


\subsubsection{\label{stf}Command Start, Command Type and Command Finish}

The requirements document says that EXP can detect the beginning of a new command and when that happens it has to wait 500 ms for the rest of the command, and if it does not arrive EXP shall wait for another new command.

The following operations are offered by EXP so OBDH can execute them to transmit its commands.

\begin{schema}{CommandStartOk}
\Delta ExpState; \Xi Memory; \Xi Time
\where
waiting = no \\
waiting' = yes \\
acquiring' = acquiring \\
sending' = sending \\
dumping' = dumping \\
waitsignal' = waitsignal \\
mode' = mode \\
ccmd' = ccmd
\end{schema}

\begin{zed}
CommandStartE == [\Xi ExpState | waiting = yes]
\end{zed}

\begin{zed}
CommandStart == (CommandStartOk \land Set500msTimer) \lor CommandStartE
\end{zed}

When EXP is waiting, OBDH can set the type of the command it wants to execute on EXP. Here we have three because there are two commands that send their responses in more than one step. These commands are: transmit data ($TD$) and memory dump ($MD$).

\begin{multicols}{2}
\begin{schema}{CommandTypeOk1}
\Delta ExpState; \Xi Memory; \Xi Time \\
type?: CTYPE
\where
waiting = yes \\
type? \notin \{TD, MD\} \\
ccmd' = type? \\
waiting' = waiting \\
acquiring' = acquiring \\
sending' = sending \\
dumping' = dumping \\
waitsignal' = waitsignal \\
mode' = mode
\end{schema}

\begin{schema}{CommandTypeOk2}
\Delta ExpState; \Xi Memory; \Xi Time \\
type?: CTYPE
\where
waiting = yes \\
type? = TD \\
ccmd' = type? \\
sending' = yes \\
acquiring' = acquiring \\
waiting' = waiting \\
dumping' = dumping \\
waitsignal' = waitsignal \\
mode' = mode
\end{schema}

\begin{schema}{CommandTypeOk3}
\Delta ExpState; \Xi Memory; \Xi Time \\
type?: CTYPE
\where
waiting = yes \\
type? = MD \\
ccmd' = type? \\
dumping' = yes \\
sending' = sending \\
acquiring' = acquiring \\
waiting' = waiting \\
waitsignal' = waitsignal \\
mode' = mode
\end{schema}

\end{multicols}

\begin{zed}
CommandTypeE == [\Xi ExpState | waiting \neq yes]
\end{zed}

\begin{zed}
CommandType == 
  CommandTypeOk1 \lor CommandTypeOk2 \lor CommandTypeOk3 \lor CommandTypeE
\end{zed}

In some way we assume that OBDH communicates to EXP that the current command has finished, but EXP will pay attention to this signal if it is waiting for it.

\begin{schema}{CommandFinishOk}
\Delta ExpState; \Xi Memory; \Xi Time
\where
waiting = yes \\
waiting' = no \\
sending' = sending \\
acquiring' = acquiring \\
dumping' = dumping \\
waitsignal' = waitsignal \\
mode' = mode \\
ccmd' = ccmd
\end{schema}

\begin{zed}
CommandFinishE == [\Xi ExpState | waiting \neq yes]
\end{zed}

\begin{zed}
CommandFinish == 
  (CommandFinishOk \land Stop500msTimer) \lor CommandFinishE
\end{zed}

Note that both $CommandFinishOk$ and $Timeout500ms$ can change the value of $waiting$. Precisely, the behaviour of EXP with respect to whether the rest of a command or a new one are accepted, depends on which of these operations is executed first.

Once the command is completed by OBDH, EXP can execute the body of the corresponding internal operation (shown in sections \ref{soo} and \ref{coo}).


\subsubsection{\label{dao}Data Acquisition}

Once OBDH orders EXP to enable the interruption for data acquisition, EXP will send the interruption to the telescope every 700 ms. Once the interruption was sent, EXP is interrupted by the telescope when there is data to be retrieved. This data is stored in a 43 b buffer. $RetrieveEData$ is, thus, EXP's  operation executed by the interruption sent by the telescope.

\begin{schema}{RetrieveEDataOk}
\Delta ExpState; \Xi Time; \Xi Status \\
data?: \seq MDATA 
\where
data? \neq \langle \rangle \\
mep + \# data? \leq 43 \\
memd' = memd \oplus \{i:1 \upto \# data? @ mep + i \mapsto data?~i\} \\
mep' = mep + \# data? + 1\\
ped' = 0 \\
mdp' = mdp \\
memp' = memp
\end{schema}

\begin{zed}
RetrieveEDataE == 
  [\Xi ExpState; data?: \seq MDATA | 
    data? = \langle \rangle \lor 43 < mep + \# data?]
\end{zed}

\begin{zed}
RetrieveEData == RetrieveEDataOk \lor RetrieveEDataE
\end{zed}

We have not included $waiting = no \land acquiring = yes$ in the precondition because $acquiring = no$ means that the telescope is not asked to acquire data, and this implies that the telescope will not send the interruption to EXP.


\subsubsection{\label{soo}Simple OBDH Commands}

In this section we show the specification of those commands that need one-step responses to finish.

The command to reset the micro-controller does not change the state of EXP because the micro-controller is considered to be an external component. Then, we (sub)specify an interface that our operation shall call to actually reset the micro-controller. Maybe this external operation can reset, for instance, the memory of our system but this is not in the requirements document.

\begin{multicols}{2}
\begin{schema}{MicroReset}
x:\num
\end{schema}

\begin{schema}{ResetMicroOk}
\Xi ExpState \\
rsp!:RTYPE
\where
waiting = no \\
ccmd = RM \\
rsp! = MR
\end{schema}
\end{multicols}

\begin{zed}
ResetMicroE == [\Xi ExpState | waiting \neq no \lor ccmd \neq RM]
\end{zed}

\begin{zed}
ResetMicro == (ResetMicroOk \land MicroReset) \lor ResetMicroE
\end{zed}


The command asking for the current time sends the current value of $ctime$.

\begin{schema}{SendClockOk}
\Xi ExpState \\
rsp!:RTYPE \\
rdata!: TIME
\where
waiting = no \\
ccmd = SC \\
rsp! = CD \\
rdata! = ctime
\end{schema}

\begin{zed}
SendClockE == [\Xi ExpState | waiting \neq no \lor ccmd \neq SC]
\end{zed}

\begin{zed}
SendClock == SendClockOk \lor SendClockE
\end{zed}

Data acquisition is performed in two steps: first, OBDH sends to EXP the appropriate command, and second, and internal EXP operation is executed every 700 ms. This internal operation sends an interruption to the telescope meaning that it has to acquire data. The first step is formalized in schema $InitDataAcq$, and the second in schema $InterruptTele$.

\begin{schema}{InitDataAcqOk}
\Delta ExpState; \Xi Memory; \Xi Time \\
rsp!:RTYPE
\where
waiting = no \\
ccmd = IDA \\
acquiring = no \\
acquiring' = waitsignal' = yes \\
rsp! = MR \\
waiting' = waiting \\
sending' = sending \\
dumping' = dumping \\
mode' = mode \\
ccmd' = ccmd
\end{schema}

\begin{zed}
InitDataAcqE == 
  [\Xi ExpState | 
    waiting \neq no \lor ccmd \neq IDA \lor acquiring \neq no]
\end{zed}

\begin{zed}
InitDataAcq == (InitDataAcqOk \land Set700msTimer) \lor InitDataAcqE
\end{zed}

When the 700 ms timer timeouts EXP internal operation $InterruptTele$ is enabled. $InterruptTele$ disables it self after executing but sets the 700 ms timer again to be woke up until OBDH tells EXP to not acquire more data.

\begin{schema}{InterruptTeleOk}
\Delta ExpState; \Xi Memory; \Xi Time \\
signal!:\nat
\where
waiting = no \\
acquiring = yes \\
waitsignal = no \\
waitsignal' = yes \\
signal! = 1 \\
waiting' = waiting \\
sending' = sending \\
dumping' = dumping \\
acquiring' = acquiring \\
mode' = mode \\
ccmd' = ccmd
\end{schema}

\begin{zed}
InterruptTele == InterruptTeleOk \land Set700msTimer
\end{zed}


\begin{schema}{StopDataAcqOk}
\Delta ExpState; \Xi Memory; \Xi Time \\
rsp!:RTYPE
\where
waiting = no \\
ccmd = SDA \\
acquiring = yes \\
acquiring' = waitsignal' = no \\
rsp! = MR \\
waiting' = waiting \\
sending' = sending \\
dumping' = dumping \\
mode' = mode \\
ccmd' = ccmd
\end{schema}

\begin{zed}
StopDataAcqE == 
  [\Xi ExpState | 
    waiting \neq no \lor ccmd \neq SDA \lor acquiring \neq yes]
\end{zed}

\begin{zed}
StopDataAcq == StopDataAcqOk \lor StopDataAcqE
\end{zed}


\begin{schema}{ReconfigOk}
\Delta ExpState; \Xi Memory; \Xi Time \\
nmode?:CMODE \\
rsp!:RTYPE \\
\where
waiting = no \\
ccmd = RC \\
mode' = nmode? \\
rsp! = MR \\
acquiring' = acquiring \\
sending' = sending \\
waiting' = waiting \\
dumping' = dumping \\
waitsignal' = waitsignal \\
ccmd' = ccmd
\end{schema}

\begin{zed}
ReconfigE == [\Xi ExpState | waiting \neq no \lor ccmd \neq RC]
\end{zed}

\begin{zed}
Reconfig == ReconfigOk \lor ReconfigE
\end{zed}


\begin{schema}{MemoryDumpOk}
\Xi ExpState \\
rsp!:RTYPE
\where
waiting = no \\
ccmd = MD \\
rsp! = MR
\end{schema}

\begin{zed}
MemoryDumpE == [\Xi ExpState | waiting \neq no \lor ccmd \neq MD]
\end{zed}

\begin{zed}
MemoryDump == MemoryDumpOk \lor MemoryDumpE
\end{zed}


\begin{schema}{MemoryLoadOk1}
\Delta ExpState; \Xi Time; \Xi Status \\
addr?:\nat \\
data?: \seq MDATA \\
rsp!:RTYPE \\
\where
waiting = no \\
ccmd = ML \\
0 < addr? \\
data? \neq \langle \rangle \\
addr? + \# data? \leq 44 \\
addr? + \# data? \leq mep \\
memd' = memd \oplus \{i:1 \upto \# data? @ i + addr? - 1 \mapsto data?~i\} \\
rsp! = MR \\
mdp' = mdp \\
mep' = mep \\
ped' = ped \\
memp' = memp
\end{schema}

\begin{schema}{MemoryLoadOk2}
\Delta ExpState; \Xi Time; \Xi Status \\
addr?:\nat \\
data?: \seq MDATA \\
rsp!:RTYPE \\
\where
waiting = no \\
ccmd = ML \\
0 < addr? \\
data? \neq \langle \rangle \\
addr? + \# data? \leq 44 \\
mep < addr? + \# data? \\
memd' = memd \oplus \{i:1 \upto \# data? @ i + addr? - 1 \mapsto data?~i\} \\
mep' = addr? + \# data? \\
rsp! = MR \\
mdp' = mdp \\
ped' = ped \\
memp' = memp
\end{schema}

\begin{zed}
MemoryLoadE1 ==
  [\Xi ExpState; addr?:\nat; data?: \seq MDATA |
    waiting \neq no \lor ccmd \neq ML]
\end{zed}

\begin{zed}
MemoryLoadE2 == \\
  \t1 [\Xi ExpState; addr?:\nat; data?: \seq MDATA; low?:BIN |
     addr? = 0 \lor data? = \langle \rangle \lor 44 < addr? + \# data?]
\end{zed}

\begin{zed}
MemoryLoad == \\
  \t1 MemoryLoadOk1 \lor MemoryLoadOk2
      \lor MemoryLoadE1 \lor MemoryLoadE2
\end{zed}


\begin{schema}{LoadParamOk}
\Xi ExpState \\
p?, actuate!:PARAM \\
rsp!:RTYPE
\where
waiting = no \\
ccmd = LP \\
actuate! = p? \\
rsp! = MR
\end{schema}

\begin{zed}
LoadParamE == [\Xi ExpState | waiting \neq no \lor ccmd \neq LP]
\end{zed}

\begin{zed}
LoadParam == LoadParamOk \lor LoadParamE
\end{zed}


%\pagebreak
\begin{multicols}{2}
\begin{schema}{TransDataOk1}
\Delta ExpState; \Xi Time \\
rsp!:RTYPE \\
rdata!: \seq MDATA
\where
waiting = no \\
ccmd = TD \\
sending = yes \\
dumping = no \\
ped = 0 \\
mep = 43 \\
rsp! = EDP \\
rdata! = memd \\
ped' = 43 \\
sending' = no \\
acquiring' = acquiring \\
waiting' = waiting \\
dumping' = dumping \\
waitsignal' = waitsignal \\
ccmd' = ccmd \\
mode' = mode \\
memp' = memp \\
memd' = memd \\
mdp' = mdp \\
mep' = mep
\end{schema}

\begin{schema}{TransDataE1}
\Xi ExpState \\
rsp!:RTYPE \\
\where
waiting = no \\
ccmd = TD \\
sending = yes \\
dumping = no \\
mep < 43 \lor ped = 43 \\
rsp! = ND
\end{schema}
\end{multicols}


\begin{multicols}{2}
\begin{schema}{TransDataOk2}
\Delta ExpState; \Xi Time; \Xi Status \\
rsp!:RTYPE \\
rdata!: \seq MDATA
\where
waiting = no \\
ccmd = TD \\
sending = dumping = yes \\
mdp < 5*43 \\
rsp! = MDD \\
rdata! = (mdp + 1 \upto mdp + 43) \dres memp \\
mdp' = mdp + 43 \\
memp' = memp \\
memd' = memd \\
mep' = mep \\
ped' = ped
\end{schema}

\begin{schema}{TransDataOk3}
\Delta ExpState; \Xi Time \\
rsp!:RTYPE \\
\where
waiting = no \\
ccmd = TD \\
sending = dumping = yes \\
mdp = 5*43 \\
rsp! = ND \\
mdp' = 0 \\
sending' = dumping' = no \\
memp' = memp \\
memd' = memd \\
mep' = mep \\
ped' = ped \\
acquiring' = acquiring \\
waiting' = waiting \\
sending' = sending \\
waitsignal' = waitsignal \\
mode' = mode \\
ccmd' = ccmd
\end{schema}

\end{multicols}

\begin{zed}
TransDataE2 == 
  [\Xi ExpState | 
    waiting \neq no \lor ccmd \neq TD \lor sending \neq yes]
\end{zed}

\begin{zed}
TransData == \\
  \t1 TransDataOk1 \\
  \t1 \lor TransDataOk2 \\
  \t1 \lor TransDataOk3 \\
  \t1 \lor TransDataE1 \\
  \t1 \lor TransDataE2
\end{zed}
