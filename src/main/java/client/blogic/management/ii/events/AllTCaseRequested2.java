package client.blogic.management.ii.events;


import java.util.List;

/**
 * @author Joaquin Mesuro
 */
public class AllTCaseRequested2 extends Event_{

	public List<Event_> getTCaseRequestedList() {
		return tCaseRequestedList;
	}

	private List<Event_> tCaseRequestedList;

	public AllTCaseRequested2(){
		super.setEventName("allTCaseRequested2");
	}

	public void setTCaseRequestedList(List<Event_> tCaseRequestedList) {
		this.tCaseRequestedList = tCaseRequestedList;
	}

	public AllTCaseRequested2(List<Event_> tCaseRequestedList){
		this.tCaseRequestedList = tCaseRequestedList;
		super.setEventName("allTCaseRequested2");
	}



} 
