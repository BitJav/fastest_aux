package client.blogic.testing.atcal.apl;

import java.util.Iterator;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

/**
 * Represents an APL set
 * 
 * @author Javier Bonet on 21/09/2019
 */
public class SetExpr implements APLExpr, Iterable<APLExpr> {

	private final ImmutableSet<APLExpr> set;

	public SetExpr(ImmutableSet<APLExpr> set) {
		this.set = set;
	}

	public SetExpr(APLExpr[] set) {
		this.set = ImmutableSet.copyOf(set);
	}

	public SetExpr(Set<APLExpr> set) {
		this.set = ImmutableSet.copyOf(set);
	}

	private static final class EmptySetHolder {
		static final SetExpr emptySet = new SetExpr(Sets.newHashSet());
	}

	public static SetExpr getEmptySet() {
		return EmptySetHolder.emptySet;
	}

	public boolean contains(APLExpr expr) {
		if (expr == null)
			return false;
		else
			return set.contains(expr);
	}

	public int size() {
		return set.size();
	}

	public APLExpr get(int n) {
		return set.asList().get(n);
	}

	public SetExpr intersection(SetExpr s) {
		return new SetExpr(Sets.intersection(this.set, s.set));
	}

	public SetExpr difference(SetExpr s) {
		return new SetExpr(Sets.difference(this.set, s.set));
	}

	public SetExpr union(SetExpr s) {
		return new SetExpr(Sets.union(this.set, s.set));
	}

	@Override
	public int hashCode() {
		final int prime  = 31;
		int       result = 1;
		result = prime * result + ((set == null) ? 0 : set.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetExpr other = (SetExpr) obj;
		if (set == null) {
			if (other.set != null)
				return false;
		} else if (!set.equals(other.set))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "{" + set + "}";
	}

	@Override
	public Iterator<APLExpr> iterator() {
		return set.iterator();
	}

}
