package client.blogic.testing.atcal;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import client.blogic.testing.atcal.apl.APLExpr;
import client.blogic.testing.atcal.apl.MapExpr;
import client.blogic.testing.atcal.apl.SetExpr;
import client.blogic.testing.atcal.z.ast.ZExpr;
import client.blogic.testing.atcal.z.ast.ZExprSet;
import client.blogic.testing.atcal.z.ast.ZMapExpr;

/**
 * Created by cristian on 4/21/15.
 */
public class ContractType extends ATCALType {
    private final String module;
    private final String constructor;
    private final List<String> constArgs;
    private final String setter;
    private final List<String> setterArgs;
    private final String getter;
    private final List<String> getterArgs;

    public ContractType(String constructor, List<String> constArgs, String setter, List<String> setterArgs,
                        String getter, List<String> getterArgs) {
        this.module = null;
        this.constructor = constructor;
        this.constArgs = constArgs;
        this.setter = setter;
        this.setterArgs = setterArgs;
        this.getter = getter;
        this.getterArgs = getterArgs;
    }

    public ContractType(String module, String constructor, List<String> constArgs, String setter, List<String> setterArgs,
                        String getter, List<String> getterArgs) {
        this.module = module.replaceAll("\"", "");  // remove string characters
        this.constructor = constructor;
        this.constArgs = constArgs;
        this.setter = setter;
        this.setterArgs = setterArgs;
        this.getter = getter;
        this.getterArgs = getterArgs;
    }

    public String getModule() {
        return module;
    }

    public String getConstructor() {
        return constructor;
    }

    public List<String> getConstArgs() {
        return constArgs;
    }

    public String getSetter() {
        return setter;
    }

    public List<String> getSetterArgs() {
        return setterArgs;
    }

    public String getGetter() {
        return getter;
    }

    public List<String> getGetterArgs() {
        return getterArgs;
    }
    

    @Override
	public APLExpr fromZExpr(ZExpr zExpr) {
		// TODO Auto-generated method stub
    	if (zExpr instanceof ZExprSet) {
            ZExprSet setZExpr = (ZExprSet) zExpr;
            Iterator<ZExpr> setIterator = setZExpr.iterator();
			Set<APLExpr>    resultSetElements = new HashSet<>();
			while (setIterator.hasNext()) {
				ZExpr setMember = setIterator.next();
				if (setMember instanceof ZMapExpr) {
					ZMapExpr mapExpr = (ZMapExpr) setMember;
					String   name    = mapExpr.getName();
					Long     value   = mapExpr.getValue();
					resultSetElements.add(new MapExpr(name, value));
				} else {
					throw new RuntimeException("Unsupported operation - ContractType - 1.");
				}
			}

			return new SetExpr(resultSetElements);
		} else {
			return super.fromZExpr(zExpr);
		}
	}

	@Override
    public String toString() {
        return "ContractType{" +
                "constructor='" + constructor + '\'' +
                ", constArgs=" + constArgs +
                ", setter='" + setter + '\'' +
                ", setterArgs=" + setterArgs +
                ", getter='" + getter + '\'' +
                ", getterArgs=" + getterArgs +
                '}';
    }
}
