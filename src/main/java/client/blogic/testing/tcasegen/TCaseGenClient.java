package client.blogic.testing.tcasegen;

import java.awt.*;
import java.io.PrintWriter;
import java.util.*;
import java.util.List;

import client.blogic.management.Controller;
import client.blogic.management.ii.events.*;
import common.z.AbstractTCase;
import compserver.tcasegen.TCaseGen;
import net.sourceforge.czt.z.ast.Spec;
import client.blogic.management.ii.EventAdmin;
import client.blogic.management.ii.IIComponent;
import common.fastest.FastestUtils;
import common.z.SpecUtils;
import common.z.TClass;
import compserver.tcasegen.strategies.SetLogStrategy;
import compserver.tcasegen.strategies.TCaseStrategy;
import client.presentation.ClientTextUI;
/**
 * Intances of this class (although we assume there is only one in the system)
 * manages the requests for abstract test cases generation.  These requests are
 * done in each event of type TCaseRequested that is announced in the system and
 * each of them are processed in a different thread (to favour performance 
 * issues), running the method run() of TCaseGenClientRunner in each new thread.
 * @author Pablo Rodriguez Monetti
 */
public class TCaseGenClient extends IIComponent {

	private Spec spec;
	private Map<String, TCaseStrategy> tCaseStrategyMap = new HashMap<String, TCaseStrategy>();

//
//	private void launchEventList(List<Event_> event_List) {
//
//		List<TClass> tClassList = new ArrayList<>();
//		List<String> opNames = new ArrayList<String>();
//		for (Event_ event_ : event_List) {
//
//			TCaseRequested tCaseRequested = (TCaseRequested) event_;
//			String opName = tCaseRequested.getOpName();
//			TClass tClass = tCaseRequested.getTClass();
//
//			String axDef = FastestUtils.allNonBasicAxDefReplaced(tClass.getMyAxPara(), myClientUI.getMyController());
//			if (axDef != null) {
//				PrintWriter output = ((ClientTextUI) myClientUI).getOutput();
//				output.println("Error: Missing value for \"" + axDef + "\" in " + SpecUtils.getAxParaName(tClass.getMyAxPara()));
//				TCaseGenerated tCaseGenerated = new TCaseGenerated(opName, tClass, null);
//				try {
//					EventAdmin eventAdmin = EventAdmin.getInstance();
//					eventAdmin.announceEvent(tCaseGenerated);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			} else {
//
//
//				tClassList.add(tClass);
//				opNames.add(opName);
//
//			}
//		}
//
//		List<AbstractTCase> abstractTClasList = (new SetLogStrategy(myClientUI)).generateAllAbstractTCase(spec,tClassList);
//
//		int i = 0;
//		List<TCaseGenerated> tCaseGeneratedList = new ArrayList<TCaseGenerated>();
//		for (AbstractTCase abstractTCase : abstractTClasList){
//			TCaseGenerated tCaseGenerated = new TCaseGenerated(opNames.get(i), tClassList.get(i), abstractTCase);
//			tCaseGeneratedList.add(tCaseGenerated);
//			i++;
//
//		}
//		try {
//			AllTCasesGenerated2 allTCasesGenerated2 = new AllTCasesGenerated2();
//			EventAdmin eventAdmin = EventAdmin.getInstance();
//			allTCasesGenerated2.settCaseGeneratedList(tCaseGeneratedList);
//			eventAdmin.announceEvent(allTCasesGenerated2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//
//	}

	private void launchEvent(Event_ event_) {
		TCaseRequested tCaseRequested = (TCaseRequested) event_;
		String opName = tCaseRequested.getOpName();
		TClass tClass = tCaseRequested.getTClass();

		String axDef = FastestUtils.allNonBasicAxDefReplaced(tClass.getMyAxPara(), myClientUI.getMyController());
		if (axDef != null) {
			PrintWriter output = ((ClientTextUI) myClientUI).getOutput();
			output.println("Error: Missing value for \"" + axDef + "\" in " + SpecUtils.getAxParaName(tClass.getMyAxPara()));
			TCaseGenerated tCaseGenerated = new TCaseGenerated(opName, tClass, null);
			try {
				EventAdmin eventAdmin = EventAdmin.getInstance();
				eventAdmin.announceEvent(tCaseGenerated);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {


			String tClassName = tClass.getSchName();

			TCaseStrategy tCaseStrategy = tCaseStrategyMap.get(tClassName);
			if (tCaseStrategy == null) {
				tCaseStrategy = new SetLogStrategy(myClientUI);
			}

			if (spec == null)
				return;

			AbstractTCase abstractTCase = TCaseGen.generateAbstractTCase(spec, tClass, tCaseStrategy);
			TCaseGenerated tCaseGenerated = new TCaseGenerated(opName, tClass, abstractTCase);
			try{
				EventAdmin eventAdmin = EventAdmin.getInstance();
				eventAdmin.announceEvent(tCaseGenerated);
			}
			catch(Exception e){
				e.printStackTrace();
			}

		}
	}

	/**
	 * Manages an implicit invocation event.
	 * @param event_
	 * @throws java.lang.IllegalArgumentException if event_ is neither instance of
	 * SpecLoaded nor TCaseRequested. 
	 */
	public synchronized void manageEvent(Event_ event_)
			throws IllegalArgumentException{

		if(event_ instanceof UnfoldedSpecLoaded){
			spec = ((UnfoldedSpecLoaded)event_).getSpec();
		}
		else if(event_ instanceof TCaseRequested){
				launchEvent(event_);
		}
		else if(event_ instanceof AllTCaseRequested2){

			List<Event_> tCaseRequestedList = ((AllTCaseRequested2) event_).getTCaseRequestedList();
			//launchEventList(tCaseRequestedList);
			for (Event_ event_1 : tCaseRequestedList){
				launchEvent(event_1);
			}
		}

		else if(event_ instanceof TCaseStrategySelected){
			TCaseStrategySelected tCaseStrategySelected = (TCaseStrategySelected)event_;
			String tClassName = tCaseStrategySelected.getTClassName();
			TCaseStrategy tCaseStrategy = tCaseStrategySelected.getTCaseStrategy();
			tCaseStrategyMap.put(tClassName, tCaseStrategy);
		}
		else if(event_ instanceof FastestResetted){
			tCaseStrategyMap = new HashMap<String, TCaseStrategy>();
		}
		else
			throw new IllegalArgumentException();
	}
}

