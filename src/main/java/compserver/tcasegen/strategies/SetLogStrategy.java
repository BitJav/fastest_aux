package compserver.tcasegen.strategies;

import java.io.IOException;
import java.util.*;

import client.blogic.testing.ttree.TClassNode;
import client.blogic.testing.ttree.visitors.TClassNodeFinder;
import client.presentation.ClientUI;
import client.presentation.ClientTextUI;
import net.sourceforge.czt.z.ast.*;

import java.util.Iterator;
import java.util.Collection;
import common.z.AbstractTCase;
import common.z.AbstractTCaseImpl;
import common.z.SpecUtils;
import common.z.TClass;
import common.z.TClassImpl;
import common.z.czt.visitors.PreExprExtractor;
import principal.Z2SL;


/* Estrategia que hace uso de SetLog para generar los casos. El parseo de Z a SetLog esta hecho basado en el codigo
 * que se utiliza en ANTLRv3 distinto al que se usa en TestRing (ANTLRv4) el cual tiene un procedimiento un poco difrente
 */
public final class SetLogStrategy implements TCaseStrategy{

	private ClientUI clientUI;
	private Z2SL z2SL;

	public SetLogStrategy(ClientUI clientUI) {
		this.clientUI = clientUI;
	}

//
//	public List<AbstractTCase> generateAllAbstractTCase(Spec spec, List<TClass> tClassList)  {
//
//		for (TClass tclass : tClassList){
//			((ClientTextUI)clientUI).getOutput().println("Trying to generate a test case for the class: " + tclass.getSchName());
//		}
//
//
//		List<AbstractTCase> abstractTCaseList = generarCasos(tClassList);
//		// si quiero ver las salida de setlog
//
//
//		return abstractTCaseList;
//
//	}

	public AbstractTCase generateAbstractTCase(Spec spec, TClass tClass)  {
		((ClientTextUI)clientUI).getOutput().println("Trying to generate a test case for the class: " + tClass.getSchName());
		AbstractTCase abstractTCase= generarCaso(tClass);
		// si quiero ver las salida de setlog
		if (clientUI.getMyController().getSetlogPrint() == true){
			((ClientTextUI) clientUI).getOutput().println( "** CLASS **" + SpecUtils.termToLatex(tClass));
			((ClientTextUI) clientUI).getOutput().println(z2SL.getSetLogCodePP());
		}
		return abstractTCase;

	}
//
//
//	private List<AbstractTCase> generarCasos(List<TClass> tClassList){
//
//		z2SL = new Z2SL();
//		AbstractTCase abstractTCase = null;
//		List<AbstractTCase> abstractTCaseList = new ArrayList<AbstractTCase>();
//
//		try {
//			Spec spec = clientUI.getMyController().getOriginalSpec();
//
//			List<AbstractTCase> abstractTCaseList1 = new ArrayList<AbstractTCase>();
//
//			List<AxPara> axParaListAux = new ArrayList<AxPara>();
//			for (TClass tClass : tClassList)
//				axParaListAux.add(tClass.getMyAxPara());
//
//			List<AxPara> axParaList =  z2SL.run(axParaListAux, spec);
//
//			int i = 0;
//			for (AxPara axPara : axParaList){
//				if (z2SL.getSlOutErrList().get(i).equals("OK")  )
//
//					abstractTCase = new AbstractTCaseImpl(axPara, tClassList.get(i).getSchName(),(z2SL.getVarExprMapList()).get(i));
//				else if (z2SL.getSlOutErr().equals("FALSE") )
//					abstractTCase = new AbstractTCaseImpl(axPara, tClassList.get(i).getSchName());
//
//				else if (axPara == null) //No encontro caso
//					return null;
//
//				//integrate(tClass,abstractTCase);//integra el caso si tiene incls (Tesina de Joaquín 1 hablar con Maxi si opcional).
//				abstractTCaseList.add(abstractTCase);
//				i++;
//			}
//			return abstractTCaseList;
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

//
//
//	private List<AbstractTCase> generarCasos(List<TClass> tClassList){
//
//		z2SL = new Z2SL();
//		AbstractTCase abstractTCase = null;
//		List<AbstractTCase> abstractTCaseList = new ArrayList<AbstractTCase>();
//
//		try {
//			Spec spec = clientUI.getMyController().getOriginalSpec();
//
//
//			for (TClass tClass : tClassList){
//				AxPara axPara = z2SL.run(tClass.getMyAxPara(),spec); //genero el caso con el motor setlog
//
//				if (z2SL.getSlOutErr().equals("OK")  )
//					abstractTCase = new AbstractTCaseImpl(axPara, tClass.getSchName(),z2SL.getVarExprMap());
//				else if (z2SL.getSlOutErr().equals("FALSE") )
//					abstractTCase = new AbstractTCaseImpl(axPara, tClass.getSchName());
//
//				else if (axPara == null) //No encontro caso
//					return null;
//
//				//integrate(tClass,abstractTCase);//integra el caso si tiene incls (Tesina de Joaquín 1 hablar con Maxi si opcional).
//				abstractTCaseList.add(abstractTCase);
//			}
//			return abstractTCaseList;
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}


	private AbstractTCase generarCaso(TClass tClass){

		z2SL = new Z2SL();
		AbstractTCase abstractTCase = null;

		try {
			Spec spec = clientUI.getMyController().getOriginalSpec();
			AxPara axPara = z2SL.run(tClass.getMyAxPara(),spec); //genero el caso con el motor setlog

			if (z2SL.getSlOutErr().equals("OK")  )
				abstractTCase = new AbstractTCaseImpl(axPara, tClass.getSchName(),z2SL.getVarExprMap());
			else if (z2SL.getSlOutErr().equals("FALSE") )
				abstractTCase = new AbstractTCaseImpl(axPara, tClass.getSchName());

			else if (axPara == null) //No encontro caso
				return null;

			//integrate(tClass,abstractTCase);//integra el caso si tiene incls (Tesina de Joaquín 1 hablar con Maxi si opcional).
			return abstractTCase;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	
	
	//supongamos A == B \land C entonces una clase de prueba de A es
	// A==[decl|\pre B \land \pre C]
	/*Hay que arreglar esta función para que sea coherente con la integración
	 * definida en el paper del TTF de integración
	 * Si por ejemplo se hace: selop B, selop A, genalltt, genalltca
	 * Antes de la linea
	 * axPara = SpecUtils.createAxPara(declList,SpecUtils.andPreds(casePred, classPred));
	 * hay q sacar de casePred las variables que sólo aparecen en B
	 * es decir, sólo dejar las asignaciones en vars(B) \cap vars(C) */
	private void integrate(TClass tClass, AbstractTCase abstractTCase){
		if (abstractTCase==null || abstractTCase.getMyAxPara() == null)
			return;
		List<PreExpr> inclPreds = tClass.getInclPreds();
		if (inclPreds == null || inclPreds.isEmpty())
			return;
		List<String> inclsNotIntegrated = new LinkedList<>();
		Iterator<PreExpr> it = inclPreds.iterator();
		PreExpr pre;
		String opName; //nombre de la operacion incluida como \pre B
		Map<String, TClassNode> opTTreeMap = clientUI.getMyController().getOpTTreeMap();
		TClassNode vis,tClassNode;
		Collection<TClassNode> tClassNodeLeaves;
		Iterator<TClassNode> tClassNodeIt;
		DeclList declList = SpecUtils.getAxParaListOfDecl(tClass.getMyAxPara()); //declaracion de A
		Pred classPred,casePred,newPred; //predicado B
		AxPara axPara;
		casePred = SpecUtils.getAxParaPred(abstractTCase.getMyAxPara()); //predicado del caso de A
		AbstractTCase newAbstractTCase;
		while (it.hasNext()){ //iteramos sobre los esuqemas incluidos (B y C)
			pre = it.next();
			opName = SpecUtils.termToLatex(pre.getExpr());
			inclsNotIntegrated.add(opName); //por defecto no integra
			vis = opTTreeMap.get(opName);
			tClassNodeLeaves = vis.acceptVisitor(new TClassNodeFinder());
			tClassNodeIt = tClassNodeLeaves.iterator();
			while (tClassNodeIt.hasNext()) { //iteramos sobre cada clase de B o C
				tClassNode = tClassNodeIt.next(); //obtenemos una clase de preuba de B
				//obtenemos las preciondiciones de B
				classPred = SpecUtils.getAxParaPred(tClassNode.getValue().getMyAxPara());
				//creamos la clase de prueba nueva a testear
				axPara = SpecUtils.createAxPara(declList,SpecUtils.andPreds(casePred, classPred));
				
				PreExprExtractor preExtractor = new PreExprExtractor();
				newPred = SpecUtils.getAxParaPred(axPara);
				newPred.accept(preExtractor);
				//System.out.println("clase+caso:" + SpecUtils.termToLatex(axPara));
				//testeamos si el caso de A anda con el predicado de B
				TClass tclasn = new TClassImpl(axPara,tClass.getSchName());
				tclasn.setInclPreds(preExtractor.getPreds());
				newAbstractTCase = generarCaso(tclasn);
				if (newAbstractTCase!=null && newAbstractTCase.getMyAxPara() != null 
						&& newAbstractTCase.getInclsNotIntegrated().isEmpty()){
					inclsNotIntegrated.remove(opName); //sacamos de las ops con la que no puede integrar el caso
					System.out.println("clase+caso: " + tClassNode.getValue().getSchName() +"   "+ tClass.getSchName());
					break; //para esta operacion(B o C) ya integró
				}
			}
		}
		abstractTCase.setInclsNotIntegrated(inclsNotIntegrated);
	}

}